# Website Plataformas Móviles

https://plataformas-moviles.now.sh

Website construido usando [Hugo](https://gohugo.io/), deployado usando [Zeit Now](https://zeit.co/).

## Instrucciones de Build

```
hugo server
```

