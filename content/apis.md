+++
menu = "main"
title = "APIs"
weight = 60

+++

Recopilado de un listado de APIs que me van pareciendo interesante. Pueden ser utiles para desarrollar aplicaciones en torno a estas APIs.

## Información 

- [**SWAPI.co**](https://swapi.co/) [EN]: Star Wars API
- [**PokéAPI.co**](https://pokeapi.co/) [EN]: Pokémon API
- [**The Lord of the Rings API**](https://the-one-api.herokuapp.com/) [EN]: El señor de los anillos. Una API para gobernarlos a todos.
- [**An API of Ice And Fire**](https://anapioficeandfire.com/) [EN]: Sobre el mundo de Game Of Thrones.
- [**The Solar System OpenData API**](https://api.le-systeme-solaire.net/en/) [EN]: Información sobre el sistema solar.

## Interactivas

- [**Imagecharts**](https://documentation.image-charts.com/) [EN]: Una API para generar gráficos y códigos QRs.
- [**QuickChart**](https://quickchart.io/) [EN]: Otra API para generar gráficos y códigos QRs.
- [**Github**](https://developer.github.com/v3/) [EN]: API de Github, se puede acceder y buscar toda la información de proyectos dentro de la plataforma.
- [**Yes No**](https://yesno.wtf/api) [EN]: API que responde Si o No. También responde con un GIF ilustrativo.

## Almacenamiento

- [**CountAPI**](https://countapi.xyz/) [EN]: API para contar cosas. Guarda un contador online.
- [**KV DB**](https://kvdb.io) [EN]: Una mini base de datos clave valor.
- [**JSON Bin**](https://jsonbin.org) [EN]: Una mini API de almacenamiento, para cualquier dato en formato JSON.
- [**JSON Box**](https://jsonbox.io/) [EN]: Otra mini API de almacenamiento, para cualquier dato en formato JSON.

## Videojuegos

- [**Riot Games**](https://developer.riotgames.com/getting-started.html) [EN]: API de Riot Games, con info sobre el LOL (League Of Legends).
- [**OpenDota**](https://docs.opendota.com/) [EN]: API con información sobre el Dota 2.
- [**Age Of Empires II API**](https://age-of-empires-2-api.herokuapp.com/docs/) [EN]: API con información sobre el Age Of Empires II.

## Pruebas

- [**JSON Placeholder**](https://jsonplaceholder.typicode.com) [EN]: API con datos de relleno, datos placeholder.
- [**Picsum**](https://picsum.photos/) [EN]: API de imágenes de relleno. Se pueden configurar distintos parámetros de las imágenes.

## Listado de listados de APIs

Acá dejo unos listados para revisar en detalle. Seguramente no todas las APIs listadas sean utiles para nosotros (por ser muy complejas, o por ser pagas). Es cuestión de explorar.

- https://any-api.com/
- https://apis.guru/browse-apis/
- https://github.com/public-apis/public-apis
- https://public-apis.xyz/
