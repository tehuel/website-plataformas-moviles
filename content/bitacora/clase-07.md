+++
date = "2019-10-21T18:00:00-03:00"
title = "Clase 7 - APIs JSON"

+++

## Teoría

Vimos el concepto de API como interfaz de comunicacion entre distintas partes de un programa o de un sistema. Puede haber APIs de sistema, puede haber APIs web.

#### JSON

Vimos JSON como lenguaje estándar de comunicación. JSON es compatible directamente con JS, pero también hay librerias o herramientas para trabajar con JSON en casi cualquier lenguaje de programación.

El lenguaje JSON se compone principalmente de 2 estructuras: listas (arrays) y objetos (mapas clave-valor, o diccionarios).

Lista (array)
```json
[ "primero", "segundo"]
```

Objeto (clave-valor)
```json
{
  "clave": "valor",
  "otra-clave": "otro valor",
}
```

Combinaciones de tipos de datos
```json
{
   "book": [
      {
         "id": "01",
         "language": "Java",
         "edition": "third",
         "author": "Herbert Schildt"
      },
      {
         "id": "07",
         "language": "C++",
         "edition": "second",
         "author": "E.Balagurusamy"
      }
   ]
}
```

#### HTTP

HTTP y HTTPS como protocolo de red para la comunicacion de distintos sistemas.

#### jQuery

jquery GET para hacer requests HTTP y conseguir informacion.

ejemplos de sintaxis con funcion dentro de funcion, callback

```js
// ejemplo de sintaxis callback
$.get("api.com/users", function(data) {
  $("#results-container").html(data);
  alert("Carga de datos completa");
});
```

```js
// ejemplo de sintaxis con funcion definida con anterioridad
function procesarRespuestaApi(data) {
  $("#results-container").html(data);
  alert("Carga de datos completa");
}

$.get("api.com/users", procesarRespuestaApi);
```

Vimos distintas APIs de ejemplo.

## Práctica

Planteamos un ejercicio para armar un listado de usuarios con informacion obtenida a traves de `http://jsonplaceholder.typicode.com/users`. 

Para armar el listado se puede usar jQuery.

Documentación importante de jQuery:

- Cómo seleccionar elementos en el DOM: [/selecting-elements/](https://learn.jquery.com/using-jquery-core/selecting-elements/)
- Cómo trabajar con las selecciones de elementos: [/working-with-selections/](https://learn.jquery.com/using-jquery-core/working-with-selections/)
- Cómo editar lso elementos seleccionados: [/manipulating-elements/](https://learn.jquery.com/using-jquery-core/manipulating-elements/)
- Cómo recorrer listados: [/iterating/](https://learn.jquery.com/using-jquery-core/iterating/)

Documentación general de jQuery

- https://learn.jquery.com/about-jquery/how-jquery-works/
- https://api.jquery.com/
