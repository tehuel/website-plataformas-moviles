+++
date = "2019-10-28T18:00:00-03:00"
title = "Clase 8 - Ejemplo API"

+++

## Teoría

Hicimos un recorrido paso a paso de un ejemplo sobre como consultar una API externa, y como mostrar esos datos en la pantalla.

- [Ver demo de ejemplo](/api-ejemplo/)
- [Código de ejemplo](https://gitlab.com/tehuel/website-plataformas-moviles/tree/master/static/api-ejemplo/).
- [ZIP con código de ejemplo](https://gitlab.com/tehuel/website-plataformas-moviles/-/archive/master/website-plataformas-moviles-master.zip?path=static%2Fapi-ejemplo).


## Grupos y temas para el TP

Estuvimos charlando con cada uno de los grupos sobre el TP final. 

Grupo | Temática
---|---
Guanes, Zamorano, Córdoba | Consultas a API de Secretaria de Cultura
Lopez, Di Loreto | Usar APIs de resultados de clubes de futbol
Palladino, Bianco, Yaciura | Buscar info de canciones, usando API de last.fm 
Decima, Mattera, Pelozo | Listado y detalles de campeones de LoL
Fernández, Gómez Lorea | App para crear fichas de personajes D&D
Evequoz, Cozzi | Índice de paises, con posible buscador
Arevalo, Figueroa, Patricio | (no decidieron)
Ferreyra, Alcaraz, Tejerina | (no decidieron)
Lagraña | (no decidió)
Martinez Molina, Avalo | (no decidieron)
Brunengo, Barrozo, Cabral, Amato | (no decidieron)
Abracaite, Acuña, Azócar | Índice de habilidades de Pokémon
Ayala, Basoalto, Manquez, Bareiro | Listado y detalles de civilizaciones de AoE
Quiñones, Ballone | (no decidieron)
Zarratea, Contreras, Chaile | (ausentes)




