+++
date = "2019-11-11T18:00:00-03:00"
title = "Clase 10 - Clase Final"

+++

## Teoría

No se explicaron nuevos temas. Los grupos estuvieron trabajando en sus proyectos.

Agregué nuevos ejemplos a la página de ejemplos:

- [Demo de Ejemplo](/api-ejemplo/)
- [Código de Ejemplo](https://gitlab.com/tehuel/website-plataformas-moviles/tree/master/static/api-ejemplo/).
- [ZIP con código de ejemplo](https://gitlab.com/tehuel/website-plataformas-moviles/-/archive/master/website-plataformas-moviles-master.zip?path=static%2Fapi-ejemplo).

## Entrega de Proyecto

El tiempo de entrega de los proyectos es **hasta el viernes 22 de noviembre a la medianoche**.

Para la entrega, un integrante del grupo me envía el proyecto, en un archivo comprimido, a [torresbaldi.tehuel@gmail.com](mailto:torresbaldi.tehuel@gmail.com), incluyendo apellido de todos los integrantes del grupo. 

Dentro del archivo que entregan pueden agregar un documento con los detalles y aclaraciones que consideren importantes para la correción. En ese documento pueden incluir también:

- Con qué problemas se encontraron
- Qué soluciones le encontraron a esos problemas
- Qué cosas no pudieron hacer, y por qué

## Balance

Hicimos un breve balance de la materia con los chicos, donde destacaron algunas cosas positivas y negativas para el futuro de la materia:

**Positivo**

- Las clases donde la teoria y los ejemplo las vimos al mismo tiempo, se entendían mejor.
- Que se vean temas modernos y útiles les parecio algo positivo.
- La existencia de este sitio de la materia, fue algo bien recibido.
- En el poco tiempo que tuvimos de clases pudieron aprender muchas cosas nuevas, y eso estuvo bueno.
- Se contagiaron un poco del entusiasmo y la pasión con lo que ibamos viendo clase a clase. 😁👍

**Negativo**

- Las clases donde la teoría estaba muy separada de la práctica, donde primero *hablaba mucho* y después recién veian un ejemplo. Para el momento que veian el ejemplo ya no tenian presente la teoría anterior.
- Que haya habido tantos problemas al momento de conectarnos con algunas de las APIs.
- Internet, no había internet.
