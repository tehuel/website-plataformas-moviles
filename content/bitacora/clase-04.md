+++
date = "2019-09-23T18:00:00-03:00"
title = "Clase 4 - Presentando Bootstrap"

+++

## Teoría

Les pasé una carpeta con la ultima versión de bootstrap, y con la documentación para verse en modo offline.

Hicimos un recorrido por la documentación de Bootstrap, y vimos como se usan algunos componentes del framework.

Vimos la responsividad de los componentes funcionando directamente dentro de la documentación. Vimos como achicando y agrandando la ventana de la documentación se iban viendo de manera diferente los distintos componentes

Principalmente vimos `container`, `grid`, y algunos componentes con su propio JS (`carousel`, y `collapse`). 

También vimos que Bootstrap es un framework enfocado principalmente en lo visual, no tiene nada de "comportamiento". El comportamiento es algo que nosotros vamos a tener que agregar.

Comentamos algunas cosas sobre UI/UX (User Interface/User Experience).

Estuvimos bastante tiempo recorriendo la documentación, y me dijeron que estaban ansiosos por empezar a usar el framework. 😅

## Práctica

Les pedí que armemos un layout que tenga algunas diferencias entre *mobile* y *desktop*.

<img src="/bitacora/bootstrap-layout.jpg" alt="Bootstrap Layout" class="img-fluid">

En la carpeta que les pasé había un `ejemplo-1.html` con las referencias necesarias para empezar a armar una maqueta, un layout.

[Descargar archivo **bootstrap.zip** (1,8 MB)](/bitacora/bootstrap.zip)

```md

- bootstrap/
    - bootstrap-4.3-docs/
    - bootstrap-4.3.1-dist/
    - js/
    - ejemplo-1.html

```

Siguieron trabajando en el layout hasta el fin de la clase ⏰

## Referencias, Links

- [bootstrap.zip](/bitacora/bootstrap.zip)
- https://getbootstrap.com/docs/4.3/getting-started/introduction/
- https://getbootstrap.com/docs/4.3/examples/
