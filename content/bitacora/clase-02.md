+++
date = "2019-09-09T18:00:00-03:00"
title = "Clase 2 - Tipo Lista"

+++

## Teoría

Especificación del lenguaje ECMAscript. Existencia de distintas versiones del lenguaje. 

Javascript y Node.js son distintas implementaciones de la especificación.

Tipos de datos básicos del lenguaje. Listas, strings, números.

En Javascript tenemos acceso a distintas APIs dependiendo del entorno. En los navegadores tenemos acceso al DOM.

## Práctica

#### 1. Sumar los numeros positivos de un listado.

```js
sumarPositivos([1,0,-4,2]) // debe devolver 3

// ejemplo imperativo
sumarPositivos(numeros) {
  var total = 0;
  for (var i = 0; i<numeros.length; i++) {
    total = total + numeros[i];
  }
  return total;
}

// ejemplo funcional
sumarPositivos(numeros) {
  return numeros
    .filter(n => n > 0)
    .reduce((n,total) => {
      return total + n;
    }, 0);
}
```

#### 2. Ordenar alfabéticamente un listado de palabras.

```js
ordenarAlfabeticamente(["Woody", "Jessie", "Buzz", "Stretch"]) // debe devolver ["Buzz", "Jessie", "Stretch", "Woody"]
```

#### 3. Poner en mayúscula la primera letra de cada palabra de un listado de palabras.

```js
primeraMayuscula(["tina", "tessa", "tara", "sylvia"]) // debe devolver ["Tina", "Tessa", "Tara", "Sylvia"]
```

```js
// helper: función que cambia la primera letra de una palabra
cambiarPrimeraLetra(palabra, nuevaLetra) {
    return nuevaLetra + palabra.substr(1);
}
```

#### 4. Remover de un listado los elementos *"falsy"*.

```js
removerFalsies([1, 0, -1, "Pepe", null, "", undefined) // debe devolver [ 1, -1, "Pepe"]
```

#### 5. Convertir una oración/string en un listado de palabras, Convertir un listado de palabras en una oración.

```js
convertirListadoAOración(["Marshmallow", "Nougat", "Oreo", "Pie"]) // debe devolver "Marshmallow Nougat Oreo Pie"
convertirOraciónAListado("IceCreamSandwich JellyBean KitKat Lollipop") // debe devolver ["IceCreamSandwich", "JellyBean", "KitKat", "Lollipop"]
```

#### 6. Convertir un listado de palabras en una oración

En este caso la oración debe ser enumerando y separando por coma.

```js
enumerarListado(['ninja', 'samurai', 'ronin']) // debe devolver "ninja, samurai y ronin"
enumerarListado(['ninja', '', 'ronin']) // debe devolver "ninja y ronin"
enumerarListado([]) // debe devolver ""
```
