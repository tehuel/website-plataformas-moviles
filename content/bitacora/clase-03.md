+++
date = "2019-09-16T18:00:00-03:00"
title = "Clase 3 - Responsive Design"

+++

## Teoría

Mostré un diagrama general de toda la materia, dando algunas definiciones puntuales, por ejemplo

- Concepto de APIs
- Diferencia entre git y github
- Desarrollo Nativo vs Híbrido

Expliqué por que elegí el camino "híbrido" para la materia.

Concepto de diseño responsivo y adaptativo.

Concepto de un enfoque Mobile First para diseñar pagias.

Concepto de accesibilidad. También la idea de que todo se debe adaptar al dispositivo desde el cual se visualiza.

Presenté Bootstrap.

### Diseño Responsive

Etiqueta Meta Viewport

```html
<meta name="viewport" content="width=device-width, initial-scale=1">
```

Media Breakpoints, Media Queries

```css
@media (max-width: 500px){
  body {
    background-color: blue;
  }
}
@media (min-width: 501px) and (max-width: 600px) {
  body {
    background-color: red;
  }
}
@media (max-width: 601px) {
  body {
    background-color: green;
  }
}
```

Tamaño de la pantalla, Tamaño Relativo

## Práctica

No hubo. 

Creí que no ibamos a llegar con el tiempo, por lo que no dí práctica, pero finalmente sobró tiempo al final de la clase. 🤷

Les pedí que para la proxima clase lleven algunas pantallas que les gustaría armar de manera responsive con Bootstrap.

## Referencias, Links

- https://mediaqueri.es/
- https://internetingishard.com/html-and-css/responsive-design/
- https://developers.google.com/web/fundamentals/design-and-ux/responsive/
- https://developer.mozilla.org/en-US/docs/Web/CSS/@media
