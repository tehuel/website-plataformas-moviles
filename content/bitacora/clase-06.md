+++
date = "2019-10-07T18:00:00-03:00"
title = "Clase 6 - Ejercicio Entrega"

+++

## Teoría

Repetí una explicación en detalle sobre el uso de la grilla/grid en Bootstrap. 

Reforcé la idea de que las distintas clases se pueden combinar, y se van aplicando en cada uno de los breakpoints, de menor a mayor.

```html
<div class="row">
  <div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4">
    <p>Una columna con un tamaño distinto para cada uno de los breakpoints</p>
  </div>
</div>
```


Les conté que la estructura de las columnas de bootstrap siempre tiene que seguir una jerarquia. Siempre el orden es `div.row` y detro de un row después todos los `div.col`.

```html
<div class="row">
  <div class="col">
    <p>Contenido de la primera columna</p>
  </div>
  <div class="col">
    <p>Contenido de la segunda columna</p>
  </div>
</div>
```

Les conté también que se pueden anidar los row y los col

```html
<div class="row">
  <div class="col">
    <p>Contenido de la primera columna</p>
  </div>
  <div class="col">
    <p>Puedo agregar un nuevo row en esta columna</p>
    <div class="row">
      <div class="col">
        Una primera columna, dentro de la segunda columna
      </div>
    </div>
  </div>
</div>
```

Les conté de https://bootswatch.com y cómo cambiar de forma rápida el estilo de bootstrap.

## Práctica

Hicimos un layout desde 0. 

El layout consiste en 2 paginas distintas, y 3 breakpoints distintos para cada una de las páginas.

{{< figure src="/bitacora/layout-entregable-login.png" caption="Layout Wireframe de login.html" class="figure" >}}

{{< figure src="/bitacora/layout-entregable-listado.png" caption="Layout Wireframe de listado.html" class="figure" >}}

La consigna es a **entregar para antes de la próxima clase**. El email donde enviar la entrega es torresbaldi.tehuel@gmail.com

### Entregaron:

1. Lopez, Di Loreto *(07/10/19 durante la clase)* 
2. Brunengo, Barrozo, Cabral, Amato *(08/10/19 09:32)* 
3. Decima, Mattera, Pelozo *(10/10/19 14:39)* 
4. Guanes, Zamorano, Córdoba *(11/10/19 15:50)* 
5. Evequoz, Cozzi *(11/10/19 22:21)* 
6. Ferreyra, Alcaraz, Tejerina *(13/10/19 13:20)* 
7. Abracaite, Acuña, Azócar *(15/10/19 08:40)* 
8. Lagraña *(21/10/19 14:50)* 
9. Zarratea, Contreras, Chaile *(21/10/19, en clase)* 
10. Arevalo, Figueroa, Patricio *(21/10/19, en clase)* 
11. Ayala, Basoalto, Manquez, Bareiro *(21/10/19, en clase)* 
12. Quiñones, Ballone *(21/10/19, en clase)* 
13. Palladino, Bianco, Yaciura *(21/10/19, en clase)* 
14. Fernández, Gómez Lorea *(21/10/19, en clase)* 
15. Martinez Molina, Avalo *(28/10/19, 03:16)* 
