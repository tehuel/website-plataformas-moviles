+++
date = "2019-09-30T18:00:00-03:00"
title = "Clase 5 - Más Bootstrap"

+++

## Teoría

Continuamos viendo Bootstrap y trabajando en algunos ejercicios. Pasé por los bancos resolviendo dudas puntuales, en general distintas para cada grupo.

## Práctica

A algunos grupos les di layouts adicionales para que trabajen. Los layouts eran de mayor complejidad, y tenian que mostrar y ocultar cosas en distintos breakpoints.

{{< figure src="/bitacora/layout-adicional-1.png" caption="Layout Adicional #1" class="figure" >}}

{{< figure src="/bitacora/layout-adicional-2.png" caption="Layout Adicional #2" class="figure" >}}

{{< figure src="/bitacora/layout-adicional-3.png" caption="Layout Adicional #3" class="figure" >}}
