+++
date = "2019-09-02T18:00:00-03:00"
title = "Clase 1 - Hola Mundo"

+++

## Teoría

Presentación de la materia.

Presentación de JavaScript.

Lenguajes Interpretados vs Lenguajes Compilados.

Desarrolladores con perfil Frontend, Backend y Fullstack.

## Práctica

#### 1. "Hola Mundo" en Javascript. 

Crear un archivo `index.html` y otro `script.js`. Desde el archivo HTML incluir/referenciar el archivo JS.

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>"Hola Mundo" en Javascript</title>
  <script type="text/javascript" src="script.js"></script>
</head>
<body>
  <p>Abrir la consola para ver el saludo</p>
</body>
</html>
```

En el archivo JS escribir por consola el mensaje "Hola Mundo", usando el método [console.log()](https://www.w3schools.com/jsref/met_console_log.asp):

```javascript
// script.js
console.log("Hola Mundo");
```

Abrir el archivo `index.html` en el navegador. Abrir la consola, dentro de las herramientas de desarrollador del navegador.

OK 👌

---

#### 2 - Accediendo al HTML desde Javascript

Agregar contenido al archivo HTML. Agregar algunos elementos con el atributo `id` definido.

```html
<body>
  <h1 id="titulo">Titulo</h1>
  <p id="lorem">Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
</body>
```

Desde JS se puede acceder a elementos en el HTML y cambiarle sus propiedades. 

Usando el metodo [`document.getElementById()`](https://www.w3schools.com/jsref/met_document_getelementbyid.asp) podemos obtener elementos individuales.

```javascript
// guardo mi referencia al elemento en una variable elementoTitulo
var elementoTitulo = document.getElementById("titulo");
```

Podemos ver, y modificar las propiedades de los elementos HTML. Por ejemplo, para modificar el contenido, se puede usar la propiedad [`innerHTML`](https://www.w3schools.com/jsref/prop_html_innerhtml.asp)

```javascript
// veo el contenido de mi elemento
console.log(elementoTitulo.innerHTML);

// modifico el contenido del elemento
elementoTitulo.innerHTML = "Titulo Modificado";
```

OK 👍

---

#### 3 - Modificando estilos, reaccionando a eventos

Crear un archivo `style.css`, y definir algunas clases.

```css
.bold {
  font-weight: bold;
}

.spaced {
  margin: 1rem;
  padding: 1rem;
}

.success {
  background-color: #1c1;
}

.warning {
  background-color: #cc1;
}

.error {
  background-color: #c11;
}
```

Desde el archivo HTML incluir/referenciar el archivo CSS recién creado, usando la etiqueta [`<link>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link), dentro del `<head>`.

```html
<head>
  <meta charset="UTF-8">
  <title>"Hola Mundo" en Javascript</title>
  <script type="text/javascript" src="script.js"></script>
  <link href="style.css" rel="stylesheet">
</head>
```

Agregar un botón al HTML. Agregar un elemento al que se le van a aplicar distintas clases.

```html
<body>
  <button>Hacer Algo!</button>
  <p id="texto" class="spaced">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
</body>
```

Conectar los eventos del botón con código JavaScript, y desde JavaScript reaccionar a ese evento.

```html
<body>
  <button onClick="hacerAlgo()">Hacer Algo!</button>
</body>
```

```js
// script.js
function hacerAlgo(event) {
  // veo por consola toda la información que se recibe de un evento
  console.log(event);

  // le agrego una clase al elemento
  var elementoTexto = document.getElementById("texto");
  elementoTexto.classList.add("bold");
  elementoTexto.classList.remove("spaced");
}
```

OK 🤓
