+++
date = "2019-11-04T18:00:00-03:00"
title = "Clase 9 - Progresos en Proyecto"

+++

## Teoría

No se explicaron nuevos temas. Los grupos estuvieron trabajando en sus proyectos.

Hubo problemas con algunas APIs que no daban correctamente los permisos de CORS. Por ahora no encontré solución, pero voy a actualizar si la consigo.

Algunos grupos decidieron implementar su propia API para proveer de información a su aplicación 👍

Un grupo decidió usar VueJS para el manejo del frontend de la aplicación 👍
