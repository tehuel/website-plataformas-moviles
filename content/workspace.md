+++
draft = true
menu = "main"
title = "Workspace"
weight = 40

+++

Instrucciones de instalación para preparar el entorno de trabajo

Tecnologías usadas en el entorno de desarrollo para la materia.

- HTML
- CSS
- Javascript
- Apache Cordova
- Android SDK
- Gradle
