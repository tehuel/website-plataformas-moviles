+++
menu = "main"
title = "Programa"
weight = 20

+++

### Temas específicos de la materia

- Javascript
    - DOM
    - Funciones
    - Métodos
    - Tipos de Datos
        - Listas
    - Versiones del lenguaje
- Diseño Responsivo
    - Bootstrap CSS Framework
    - Mobile First
    - Bootstrap Grid
    - Bootstrap Breakpoints
- Apache Cordova
    - Estructura del proyecto
    - Diferencias con app nativa
    - Acceso a APIs nativas
- APIs
    - Concepto de API
    - HTTP APIs
        - JSON APIs
        - REST APIs
        - Paginación
        - Autenticación

---

### Temas adicionales

- Control de Versiones (git)
    - git vs subversion
    - git vs github
    - git flow
    - git hub flow (pull requests)
- Desarrollo ágil de software
    - Scrum
    - Sprints
    - Issues / User Stories
    - CI - Integración Continua
- Transpiling
    - Typescript
    - Polyfills
- Testing
    - TDD
    - Tests Unitarios
    - Tests de Integración

---
