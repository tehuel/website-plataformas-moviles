+++
draft = true
menu = "main"
title = "Proyecto"
weight = 30

+++

### Primera Iteración

- Sitio web estático
- Diseño Responsive
- Formato APK para Android
- Varias pantallas distintas

### Segunda Iteración

- Conexión a API remota
- Sitio Interactivo
    - Buscador
    - Asistente
    - Calculador
    - Filtros

### Tercera Iteración

- Uso de APIs nativas de dispositivo
    - Cámara
    - GPS
    - Archivos
    - Batería
