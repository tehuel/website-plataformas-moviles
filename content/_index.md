+++
title = "Desarrollo de Software para Plataformas Móviles"

+++

Bienvenido al sitio web de la materia Desarrollo de Software para Plataformas Móviles, de la Escuela de Educación Secundaria Técnica nº4 de Berazategui, año 2019.

Ver [Bitácora](/bitacora/) y [Recursos](/recursos/).

---

### Ejemplo API

- [Demo de Ejemplo](/api-ejemplo/)
- [Código de Ejemplo](https://gitlab.com/tehuel/website-plataformas-moviles/tree/master/static/api-ejemplo/).
- [ZIP con código de ejemplo](https://gitlab.com/tehuel/website-plataformas-moviles/-/archive/master/website-plataformas-moviles-master.zip?path=static%2Fapi-ejemplo).

---

## Docentes

- Duilio Omar Peral
- Tehuel Torres Baldi

---

## Comentarios?

Por consultas o comentarios sobre el sitio podés [crear un issue](https://gitlab.com/tehuel/website-plataformas-moviles/issues/new) en el [repositorio del sitio](https://gitlab.com/tehuel/website-plataformas-moviles/). 

Para cualquier otra consulta o comentario, podés mandar un mail a [torresbaldi.tehuel@gmail.com](mailto:torresbaldi.tehuel@gmail.com).
