+++
menu = "main"
title = "Recursos"
weight = 20

+++

Un listado de links y recursos importantes e interesantes para la materia.

### Generales

- [**Mozilla Developer Network**](https://developer.mozilla.org/) [EN/ES]: una plataforma de aprendizaje para las tecnologías Web y el software con el que funciona la Web.
- [**W3Schools**](https://www.w3schools.com/) [EN]:  The world's largest web developer site.

---

### Documentación Javascript

- [**The Modern JavaScript Tutorial**](https://javascript.info/) [EN]: Tutorial completo, de punta a punta, sobre Javascript. Recomendadísimo.
- [**MDN JavaScript**](https://developer.mozilla.org/es/docs/Web/JavaScript) [EN/ES]: Sección dedicada a JavaScript dentro de MDN, con tutoriales y artículos.

---

### Responsive Web Design

- [**Responsive Design Tutorial**](https://internetingishard.com/html-and-css/responsive-design/) [EN]: Tutorial paso a paso, desde 0, sobre el diseño responsive.
- [**MediaQueri.es**](https://mediaqueri.es/) [EN]: Galería de ejemplos de sitios web con diseño responsivo.
- [**Responsive Design Patterns (Google Web Fundamentals)**](https://developers.google.com/web/fundamentals/design-and-ux/responsive/patterns) [EN]: Galería de ejemplos de patrones de diseño responsivo comunes, con código ejemplo.

---

### Git

- [**Learn Git Concepts**](https://dev.to/unseenwizzard/learn-git-concepts-not-commands-4gjc) [EN]: Tutorial detallando los conceptos necesarios para el uso diario de Git.
